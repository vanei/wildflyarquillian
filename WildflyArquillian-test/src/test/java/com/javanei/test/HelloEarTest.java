/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javanei.test;

import com.javanei.Hello;
import java.io.File;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OverProtocol;
import org.jboss.arquillian.container.test.api.TargetsContainer;
import org.jboss.arquillian.container.test.api.Testable;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.descriptor.api.Descriptors;
import org.jboss.shrinkwrap.descriptor.api.application6.ApplicationDescriptor;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author vanei
 */
@RunWith(Arquillian.class)
public class HelloEarTest {

    @EJB
    private Hello hello;

    @Deployment
    @TargetsContainer("main-server-group")
    @OverProtocol(value = "Servlet 3.0")
    public static EnterpriseArchive createDeployment() throws Exception {
        // Carrega o jar da API
        File api = Maven.resolver().loadPomFromFile("pom.xml").resolve("com.javanei:WildflyArquillian-api").withoutTransitivity().asSingleFile();
        System.out.println("############# api: " + api);

        // Carrega o jar do EJB
        File ejb = Maven.resolver().loadPomFromFile("pom.xml").resolve("com.javanei:WildflyArquillian-ejb").withoutTransitivity().asSingleFile();
        System.out.println("############# ejb: " + ejb);

        // Cria o war de teste
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClasses(HelloEarTest.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        System.out.println("############# war: " + war.toString(false));

        // Cria o application.xml
        ApplicationDescriptor xml = Descriptors.create(ApplicationDescriptor.class)
                .applicationName("WildflyArquillian")
                .libraryDirectory("lib")
                .createModule().ejb(ejb.getName()).up()
                .createModule().getOrCreateWeb()
                .contextRoot("test")
                .webUri("test.war")
                .up()
                .up();
        System.out.println("############# xml: " + xml.exportAsString());

        // Cria o EAR
        EnterpriseArchive ear = ShrinkWrap.create(EnterpriseArchive.class, "WildflyArquillian-ejb.ear")
                //.addAsLibrary(jar)
                .addAsLibrary(api)
                .addAsModules(ejb)
                .addAsModules(Testable.archiveToTest(war))
                .setApplicationXML(new StringAsset(xml.exportAsString()));
        System.out.println("############# ear: " + ear);
        //ear.as(ZipExporter.class).exportTo(new File("WildflyArquillian-ear.ear"));

        return ear;
    }

    @Test
    @TargetsContainer("master:server-two")
    public void sayHello() throws Exception {
        System.out.println("############# hello: " + hello);
        String oi = hello.sayHello();
        Assert.assertEquals("Hello", oi);
    }
}
