/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javanei.test;

import com.javanei.Hello;
import java.io.File;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OverProtocol;
import org.jboss.arquillian.container.test.api.TargetsContainer;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author vanei
 */
@RunWith(Arquillian.class)
public class HelloWarTest {

    @EJB
    private Hello hello;

    @Deployment
    @TargetsContainer("main-server-group")
    @OverProtocol(value = "Servlet 3.0")
    public static Archive<?> createDeployment() {
        // Carrega o jar da API
        File api = Maven.resolver().loadPomFromFile("pom.xml").resolve("com.javanei:WildflyArquillian-api").withoutTransitivity().asSingleFile();
        System.out.println("@@@@@@@@@@@@@ api: " + api);

        // Carrega o jar do EJB
        File ejb = Maven.resolver().loadPomFromFile("pom.xml").resolve("com.javanei:WildflyArquillian-ejb").withoutTransitivity().asSingleFile();
        System.out.println("@@@@@@@@@@@@@ ejb: " + ejb);

        // Cria o war de teste
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClasses(HelloWarTest.class)
                .addAsLibrary(api)
                .addAsLibrary(ejb);
        System.out.println("@@@@@@@@@@@@@ war: " + war.toString(false));
        //war.as(ZipExporter.class).exportTo(new File("test.war"));

        return war;
    }

    @Test
    @TargetsContainer("master:server-two")
    public void sayHello() {
        String oi = hello.sayHello();
        Assert.assertEquals("Hello", oi);
    }
}
